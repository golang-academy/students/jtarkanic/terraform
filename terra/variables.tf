variable "image" {
  description = "Image ID to use"
  default     = "ami-0b93ce03dcbcb10f6"
}

variable "instance" {
  description = "Instance type"
  default     = "t2.micro"
}

variable "name" {
  description = "Name of instance"
}

variable "owner" {
  description = "Owner"
}

