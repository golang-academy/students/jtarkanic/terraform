#!/bin/bash
# install ngix
apt install -y nginx

systemctl enable --now nginx

echo "Hello from ec2 intance with nginx" > /var/www/html/index.html

# Create User
useradd -s /bin/bash -c "Admin" -m admin -g admin
echo "admin:Passw0rd" | chpasswd
# Set sudo
echo "admin ALL=(ALL)       NOPASSWD: ALL" >> /etc/sudoers
# Deploy SSH keys
mkdir /home/admin/.ssh
cat <<EOF | tee -a /home/admin/.ssh/authorized_keys
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDDj+mqtKu7Hr6q8XCOf+HKwejqw2CgHdKjJVZsP4bcegzH32z3OMBwrXHlhgyQhDQY2Q4YFFWD8fcaMuIHGNbLnVwLzvgpb47t+nDtzH9kT0G7uPQvtIj+/6oHuLxfrQdacOTEA8YXCWvrcLUQ5TSKeGq9VlQGKx5T+aDfxHb+ATUN/YDzwet3nqfIqEp4Kx+KDoHKnQ7Ek4dSns8940wef6jMtnBDLwsXQYy1BKBFFYXVtIBEZXNmq7Jl6K7KR+3LP1j1uNBeyRAU3DHD8VVpaKQEDpBDxlZgFcm+Fhg6xA3BPPr5F2KtT49zm+5zX1MS+hVwS8SIMsXuVx9Eytyp jozef
EOF
# Set proper permissions
chown -R admin:admin /home/admin/.ssh
chmod 700 /home/admin/.ssh
chmod 600 /home/admin/.ssh/authorized_keys
